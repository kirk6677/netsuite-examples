/*
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Profile
define(
    'Profile.Model.Extended'
    ,	[
        'Address.Collection'
        ,   'Profile.Model'
        ,	'SC.Configuration'
        ,	'underscore'
        ,	'jQuery'
        ,	'Backbone'
        ,	'Utils'
    ]
    ,	function (
        AddressCollection
        ,   ProfileModel
        ,	Configuration
        ,	 _
        ,	jQuery
        ,	Backbone
        ,	Utils
    )
    {
        'use strict';



        return _.extend( ProfileModel.prototype, {


            getSearchApiUrl: function (unbxd)
            {
                //We've got to disable passwordProtectedSite and loginToSeePrices features if customer registration is disabled.
                if (Configuration.getRegistrationType() !== 'disabled' &&
                    (Configuration.get('siteSettings.requireloginforpricing', 'F') === 'T' || Configuration.get('siteSettings.siteloginrequired', 'F') ==='T') &&
                    this.get('isLoggedIn') === 'T')
                {
                    return Utils.getAbsoluteUrl('searchApi.ssp');
                }

                if(unbxd == "True"){
                    return '/sca-dev-2019-1/unbxd.ss';
                } else {
                    //return '/api/items';
                    return '/api/items';
                }
            }

        });
    });
