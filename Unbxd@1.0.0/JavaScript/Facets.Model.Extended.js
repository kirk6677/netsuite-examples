/*
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Facets
define('Facets.Model.Extended'
    ,	[	'Backbone.CachedModel'
        ,   'Facets.Model'
        ,	'Item.Collection'
        ,	'Session'
        ,	'Profile.Model'
        ,	'SC.Configuration'

        ,	'underscore'
        ,	'Utils'
    ]
    ,	function (
        BackboneCachedModel
        ,   FacetsModel
        ,	ItemCollection
        ,	Session
        ,	ProfileModel
        ,	Configuration

        ,	_
    )
    {
        'use strict';



        return _.extend( FacetsModel.prototype, {

            options: {}

            ,	searchApiMasterOptions: Configuration.get('searchApiMasterOptions.Facets', {})

            ,	url: function()
            {

                //debug switch for unbxd
                if (window.location.search.indexOf('debug=true') > -1) {
                    //console.log('Using NS API');
                    var unbxd="False";
                } else {
                    //console.log('Using Unbxd API');
                    var unbxd="True";
                }
                //end of Debug switch


                var profile = ProfileModel.getInstance()
                    ,	url = _.addParamsToUrl(
                    profile.getSearchApiUrl(unbxd)
                    ,	_.extend(
                        (Configuration.get('matrixchilditems.enabled') && Configuration.get('matrixchilditems.fieldset')) ? {
                            matrixchilditems_fieldset: Configuration.get('matrixchilditems.fieldset')
                        } : {}
                        ,	this.searchApiMasterOptions
                        ,	Session.getSearchApiParams()
                    )
                    ,	profile.isAvoidingDoubleRedirect()
                );

                return url;
            }

        });
    });
