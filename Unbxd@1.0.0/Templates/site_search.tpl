<div>
    <div class="header-site-search-desktop" >
        <div class="site-search">
            <div class="site-search-content">
                <form class="site-search-content-form" method="GET" action="/search" data-action="search">
                    <div class="site-search-content-input">
                        <div>
                            <span class="twitter-typeahead" style="position: relative; display: inline-block; direction: ltr;">
                                <input class="itemssearcher-input typeahead tt-hint" type="search" autocomplete="off" maxlength="40" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; opacity: 1;" readonly="" spellcheck="false" tabindex="-1">
                                <input class="itemssearcher-input typeahead tt-input" placeholder="Search for products" type="search" autocomplete="off" maxlength="40" spellcheck="false" style="position: relative; vertical-align: top; background-color: transparent;" dir="auto">
                                <span class="tt-dropdown-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none; right: auto;">
                                    <div class="tt-dataset-1"></div>
                                </span>
                            </span>
                            </div>
                        <i class="site-search-input-icon"></i>
                        <a class="site-search-input-reset" data-type="search-reset">
                            <i class="site-search-input-reset-icon"></i>
                        </a>
                    </div>
                    <button class="site-search-button-submit" type="submit">Go</button>
                </form>
            </div>
        </div>
    </div>
</div>