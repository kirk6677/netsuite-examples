// @module Wisteria.PDP.PDP
define('Wisteria.PDP.Notices.View'
	,	[
		'wisteria_pdp_notices.tpl'
		,	'Utils'
		,	'Backbone'
		,	'SC.Configuration'
		,	'jQuery'
		,	'underscore'
	]
	,	function (
		wisteria_pdp_notices_tpl
		,	Utils
		,	Backbone
		,   Configuration
		,	jQuery
		,	_
	)
	{
		'use strict';

		// @class Wisteria.PDP.PDP.View @extends Backbone.View
		return Backbone.View.extend({

			template: wisteria_pdp_notices_tpl

			,	contextDataRequest: ['item']

			,      validateContextDataRequest: function()
			{
				return true;
			}

			,	initialize: function (options) {


			}

			,	events: {
			}

			,	bindings: {
			}

			, 	childViews: {

			}

			//@method getContext @return Wisteria.PDP.PDP.View.Context
			,	getContext: function getContext()
			{

				if (this.contextData.item)
				{
					var item = this.contextData.item();
				}


				//@class Wisteria.PDP.PDP.View.Context
				return {
						noHandling: Configuration.noHandlingToggle
					,	noHandlingMessage: Configuration.noHandlingMessage
					,	handlingMessage: item.keyMapping_handlingCostMessage

				};
			}
		});
	});