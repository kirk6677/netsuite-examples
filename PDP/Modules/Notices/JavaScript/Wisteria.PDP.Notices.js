
define(
	'Wisteria.PDP.Notices'
	,   [
		'Wisteria.PDP.Notices.View'
	]
	,   function (
		PDPNoticesView
	)
	{
		'use strict';

		return  {
			mountToApp: function mountToApp (container)
			{
				// using the 'Layout' component we add a new child view inside the 'Header' existing view
				// (there will be a DOM element with the HTML attribute data-view="Header.Logo")
				// more documentation of the Extensibility API in
				// https://system.netsuite.com/help/helpcenter/en_US/APIs/SuiteCommerce/Extensibility/Frontend/index.html

				/** @type {LayoutComponent} */
				var layout = container.getComponent('PDP');

				if(layout)
				{
					layout.addChildViews(
						'ProductDetails.Full.View'
						,	{
							'Product.Notices' : {
								'Notices' : {
									childViewIndex: 8
									,	childViewConstructor : function () {
										return new PDPNoticesView();
									}
								}
							}
						}
					);

					layout.addChildViews(
						'ProductDetails.QuickView.View'
						,	{
							'Product.Notices' : {
								'Notices' : {
									childViewIndex: 8
									,	childViewConstructor : function () {
										return new PDPNoticesView();
									}
								}
							}
						}
					);
				}

			}
		};
	});
