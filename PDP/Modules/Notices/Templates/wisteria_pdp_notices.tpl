{{#if handlingMessage}}
	<p class="pdp-notices pdp-handling-notice {{#if noHandling}}text-strikethrough{{/if}}">{{{handlingMessage}}}</p>
	{{#if noHandling}}
	<p class="pdp-notices pdp-no-handling-notice">{{noHandlingMessage}}</p>
	{{/if}}
{{/if}}