// @module Wisteria.PDP.Curalate
define('Wisteria.PDP.Curalate.View'
,	[
		'wisteria_pdp_curalate.tpl'
	,	'Utils'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	]
,	function (
		wisteria_pdp_curalate_tpl
	,	Utils
	,	Backbone
	,	jQuery
	,	_
	)
{
	'use strict';

	// @class Wisteria.PDP.Curalate.View @extends Backbone.View
	return Backbone.View.extend({

		template: wisteria_pdp_curalate_tpl

	,	contextDataRequest: ['item']

	,	initialize: function (options) {


			if (!SC.isPageGenerator()) {


				//Horrible hack if crl8 is already loaded then delete experience and reload to show info for product
				//otherwise load script.
				if(window.crl8){
					console.log('Its already loaded');

					window.crl8.destroyAllExperiences();

					$("#crl8-product-container").ready(function(){

						setTimeout(function(){

							window.crl8.createExperience('product');

							}, 1000);

					});

				} else {
					console.log("its not loaded yet");

					jQuery.ajax({
						url: '//cdn.curalate.com/sites/wisteria-7kga9j/site/latest/site.min.js',
						dataType: 'script',
						success: function () {

							//window.crl8.destroyExperience('product');
							//window.crl8.createExperience('product');
						}
					});
				}
			}


		}

	,	events: {
		}

	,	bindings: {
		}

	, 	childViews: {
			
		}

		//@method getContext @return Wisteria.PDP.Curalate.View.Context
	,	getContext: function getContext()
		{


			if (this.contextData.item)
			{
				var item = this.contextData.item();
			}

			//@class Wisteria.PDP.PDP.View.Context
			return {
				itemId: item.itemid
			};
		}
	});
});