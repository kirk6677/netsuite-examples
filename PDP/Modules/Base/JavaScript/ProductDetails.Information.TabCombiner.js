define('ProductDetails.Information.TabCombiner'
,	[
		'ProductDetails.Information.View'
	,	'SC.Configuration'
	,	'Utils'
	,	'underscore'
	]
,	function(
		ProductDetailsInformationView
	,	Configuration
	,	Utils
	,	_
)
{
	'use strict';

	return {
		mountToApp: function () {
			ProductDetailsInformationView.prototype.computeDetailsArea = function () {
				var self = this
				,	sections = []
				,	details = [];

				_.each(Configuration.get('productDetailsInformation', []), function (item_information)
				{
					//console.log(item_information);
					var content = '';

					if (item_information.contentFromKey)
					{
						content = self.model.get('item').get(item_information.contentFromKey);
					} else {
						if (item_information.staticMessage) {
							content = item_information.staticMessage;
						}
					}

					if (content && Utils.trim(content))
					{
						if(!~sections.indexOf(item_information.name)) {
							sections.push(item_information.name);
							details.push({
								name: item_information.name
							,	content: []
							,	itemprop: item_information.itemprop
							});
						}

						content = '<p>' + Utils.trim(content).replace('\n', '<br>') + '</p>';
						details[sections.indexOf(item_information.name)]['content'].push(content);
					}

				});

				_.each (details, function(f) {
					f['content'] = f['content'].join('\n');
				});

				return details;	
			}
		}
	}
});