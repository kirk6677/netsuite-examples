
define(
	'Wisteria.PDP.Base'
,   [
		'Wisteria.PDP.Icons',
		'Wisteria.PDP.BasicInfo',
		'Wisteria.PDP.Promo',
		'Wisteria.PDP.Notices',
		'Wisteria.PDP.Curalate',
		'ProductDetails.ImageGallery.View.Ext',
		'ProductDetails.Information.TabCombiner'
	]
,   function (
		Icons,
		BasicInfo,
		Promo,
		Notices,
		Curalate,
		ProductDetailsImageGalleryView,
		ProductDetailsInformationTabCombiner
	)
{
	'use strict';

	return  {
		mountToApp: function mountToApp (container)
		{
			//Use this as a Entry Point file and load each PDP module

			Icons.mountToApp(container);
			BasicInfo.mountToApp(container);
			Promo.mountToApp(container);

			//Not working, Trying to get Configuration loaded to make this work
			Notices.mountToApp(container);

			Curalate.mountToApp(container);

			ProductDetailsImageGalleryView.mountToApp(container);

			ProductDetailsInformationTabCombiner.mountToApp(container);

		}
	};
});
