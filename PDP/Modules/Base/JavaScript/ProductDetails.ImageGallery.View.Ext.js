/*
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module ProductDetails
define(
	'ProductDetails.ImageGallery.View.Ext'
,	[
		'ProductDetails.ImageGallery.View'
	,	'underscore'
	,	'Utils'

	,	'jQuery.bxSlider'
	]
,	function (
		ProductDetailsImageGalleryView
	,	_
	,	Utils
	)
{
	'use strict';

	return {
		mountToApp: function () {
			//console.log('ProductDetails.ImageGallery.View.Ext mountToApp');

			ProductDetailsImageGalleryView.prototype.initSlider = function () {
				var self = this;

				if (self.images.length > 1)
				{
					self.$slider = Utils.initBxSlider(self.$('[data-slider]'), {

							startSlide: 0
						,	adaptiveHeight: true
						,	touchEnabled: true
						,	nextText: '<a class="product-details-image-gallery-next-icon" data-action="next-image"></a>'
						,	prevText: '<a class="product-details-image-gallery-prev-icon" data-action="prev-image"></a>'
						, 	controls: true
						,	pager: false
						,	onSliderLoad: function () {
								this.getCurrentSlideElement().addClass('active-slide');
							}
						,	onSlideBefore: function (element, oldIndex, newIndex) {
								$('.active-slide').removeClass('active-slide')
								element.addClass('active-slide');
							}
					});

					self.$sliderpager = Utils.initBxSlider(self.$('[data-slider-pager]'), {

							startSlide: 0

						,	touchEnabled: true
						,	nextText: '<a class="product-details-image-gallery-pager-next-icon" data-action="next-pager-image"></a>'
						,	prevText: '<a class="product-details-image-gallery-pager-prev-icon" data-action="prev-pager-image"></a>'
						, 	controls: self.images.length > 4
						,	pager: false
						,	maxSlides: 4
						,	slideWidth: 100
						,	slideMargin: 10
						,	moveSlides: 1
						,	auto: false
						,	onSliderLoad: function (i) {
								self.$('[data-slider-pager] li').on('click', 'a', function(e){
									e.preventDefault();
									self.$slider.goToSlide($(this).attr('data-slideindex'));
								});
							}
					});				

					self.$('[data-action="next-image"]').off();
					self.$('[data-action="prev-image"]').off();

					self.$('[data-action="next-image"]').click(_.bind(self.nextImageEventHandler, self));
					self.$('[data-action="prev-image"]').click(_.bind(self.previousImageEventHandler, self));
				}				
			}
		}
	}
});