<div class="pdp-basic-info">
    {{#if shortDescription}}
    <p class="pdp-short-description" itemprop="description">{{{shortDescription}}}</p>
    {{/if}}
    {{#if bullets}}
    <ul class="bullets">
        {{#each bullets}}
        <li>{{this}}</li>
        {{/each}}
        {{#if dimensions}}
        <li>{{dimensions}}
            {{/if}}
            {{#if assemblyText}}
        <li>{{assemblyText}}</li>
        {{/if}}
    </ul>
    {{/if}}
</div>