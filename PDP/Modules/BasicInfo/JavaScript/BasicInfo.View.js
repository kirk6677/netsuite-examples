// @module Wisteria.PDP.PDP
define('Wisteria.PDP.BasicInfo.View'
	,	[
		'wisteria_pdp_basicinfo.tpl'
		,	'Utils'
		,	'Backbone'
		,	'jQuery'
		,	'underscore'
	]
	,	function (
		wisteria_pdp_basicinfo_tpl
		,	Utils
		,	Backbone
		,	jQuery
		,	_
	)
	{
		'use strict';

		// @class Wisteria.PDP.PDP.View @extends Backbone.View
		return Backbone.View.extend({

			template: wisteria_pdp_basicinfo_tpl

			,	contextDataRequest: ['item']

			,      validateContextDataRequest: function()
			{
				return true;
			}

			,	initialize: function (options) {


			}

			,	events: {
			}

			,	bindings: {
			}

			, 	childViews: {

			}

			//@method getContext @return Wisteria.PDP.PDP.View.Context
			,	getContext: function getContext()
			{

				if (this.contextData.item)
				{
					var item = this.contextData.item();
				}


				//@class Wisteria.PDP.PDP.View.Context
				return {
					shortDescription: item.custitem_short_description
					,	bullets: item._bullets
					,	dimensions: item.custitem_wis_product_size
					,	assemblyText: item.custitem_wis_assembly_required
				}

			}
		});
	});