// @module Wisteria.PDP.PDP
define('Wisteria.PDP.Promo.View'
	,	[
		'wisteria_pdp_promo.tpl'
		,	'Utils'
		,	'Backbone'
		,	'jQuery'
		,	'underscore'
	]
	,	function (
		wisteria_pdp_promo_tpl
		,	Utils
		,	Backbone
		,	jQuery
		,	_
	)
	{
		'use strict';

		// @class Wisteria.PDP.PDP.View @extends Backbone.View
		return Backbone.View.extend({

			template: wisteria_pdp_promo_tpl

			,	contextDataRequest: ['item']

			,      validateContextDataRequest: function()
			{
				return true;
			}

			,	initialize: function (options) {


			}

			,	events: {
			}

			,	bindings: {
			}

			, 	childViews: {

			}

			//@method getContext @return Wisteria.PDP.PDP.View.Context
			,	getContext: function getContext()
			{

				if (this.contextData.item)
				{
					var item = this.contextData.item();
				}

				//@class Wisteria.PDP.PDP.View.Context

				return {
					promoId: item.custitem_wis_promo_id
				};
			}
		});
	});