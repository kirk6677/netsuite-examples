<div class="pdp-icons">
    {{#if hasIcons}}
    <ul class="product-icons">
        {{#if isNew}}
        <!--<p class="small-title sc-highlighted">NEW!</p>-->
        <li>NEW!</li>
        {{/if}}
        {{#if exclusive}}
        <li><img src="/site/icons/WISTEX.png" alt="Wisteria Exclusive"> Wisteria Exclusive</li>
        {{/if}}
        {{#if handmade}}
        <li><img src="/site/icons/HMADE.png" alt="Handmade"> Handmade</li>
        {{/if}}
        {{#if rareBird}}
        <li><img src="/site/icons/RBIRD.png" alt="Rarebird"> RareBird</li>
        {{/if}}
    </ul>
    {{/if}}
</div>