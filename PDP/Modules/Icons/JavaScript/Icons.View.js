// @module Wisteria.PDP.PDP
define('Wisteria.PDP.Icons.View'
,	[
		'wisteria_pdp_icons.tpl'
	,	'Utils'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	]
,	function (
		wisteria_pdp_icons_tpl
	,	Utils
	,	Backbone
	,	jQuery
	,	_
	)
{
	'use strict';

	// @class Wisteria.PDP.PDP.View @extends Backbone.View
	return Backbone.View.extend({

		template: wisteria_pdp_icons_tpl

	,	contextDataRequest: ['item']

	,      validateContextDataRequest: function()
		{
			return true;
		}

	,	initialize: function (options) {

		}

	,	events: {
		}

	,	bindings: {
		}

	, 	childViews: {
			
		}

		//@method getContext @return Wisteria.PDP.PDP.View.Context
	,	getContext: function getContext()
		{

			if (this.contextData.item)
			{
				var item = this.contextData.item();
			}

			//@class Wisteria.PDP.PDP.View.Context
			return {
				isNew: item.custitem_wis_new
				,	exclusive: item.keyMapping_wistex
				,	handmade: item.custitem_wis_handmade
				,	hasIcons: item.custitem_wis_new || item.keyMapping_wistex || item.custitem_wis_handmade || item.custitem_wis_rare_bird
				,   rareBird: item.custitem_wis_rare_bird

			};
		}
	});
});