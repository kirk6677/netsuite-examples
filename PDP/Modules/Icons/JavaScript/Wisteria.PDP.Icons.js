
define(
	'Wisteria.PDP.Icons'
,   [
		'Wisteria.PDP.Icons.View'
	]
,   function (
		PDPIconsView
	)
{
	'use strict';

	return  {
		mountToApp: function mountToApp (container)
		{
			// using the 'Layout' component we add a new child view inside the 'Header' existing view
			// (there will be a DOM element with the HTML attribute data-view="Header.Logo")
			// more documentation of the Extensibility API in
			// https://system.netsuite.com/help/helpcenter/en_US/APIs/SuiteCommerce/Extensibility/Frontend/index.html

			/** @type {LayoutComponent} */
			var layout = container.getComponent('PDP');

			if(layout)
			{
				layout.addChildViews(
					'ProductDetails.Full.View'
					,	{
						'Product.Icons' : {
							'Icons' : {
								childViewIndex: 5
								,	childViewConstructor : function () {
									return new PDPIconsView();
								}
							}
						}
					}
				);

				layout.addChildViews(
					'ProductDetails.QuickView.View'
					,	{
						'Product.Icons' : {
							'Icons' : {
								childViewIndex: 5
								,	childViewConstructor : function () {
									return new PDPIconsView();
								}
							}
						}
					}
				);
			}

		}
	};
});
