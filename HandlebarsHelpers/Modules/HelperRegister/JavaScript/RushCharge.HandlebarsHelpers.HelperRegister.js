
define(
	'RushCharge.HandlebarsHelpers.HelperRegister'
,   [
		'Handlebars',
		'SC.Configuration'
	]
,   function (
		Handlebars,
		Configuration
	)
{
	'use strict';

	return  {
		mountToApp: function mountToApp (container)
		{
			var self = this;
			const isMobileHelperEnabled = Configuration.get('handlebarsHelpers.isMobile');
			if(isMobileHelperEnabled) {
				Handlebars.registerHelper('isMobile', function (options) {
					if (self.isMobileDevice()) {
						return options.fn(this);
					}
					return options.inverse(this);
				});
			}

			const isDesktopHelperEnabled = Configuration.get('handlebarsHelpers.isDesktop');
			if(isDesktopHelperEnabled) {
				Handlebars.registerHelper('isDesktop', function (options) {
					if (!self.isMobileDevice()) {
						return options.fn(this);
					}
					return options.inverse(this);
				});
			}
		},
		isMobileDevice: function(){
			return typeof window.ontouchstart === "object"
		}
	};
});
